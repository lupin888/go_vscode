go install github.com/nsf/gocode
go install github.com/rogpeppe/godef
go install github.com/zmb3/gogetdoc
go install github.com/golang/lint/golint
go install github.com/lukehoban/go-outline
go install sourcegraph.com/sqs/goreturns
go install golang.org/x/tools/cmd/gorename
go install github.com/tpng/gopkgs
go install github.com/newhook/go-symbols
go install github.com/cweill/gotests/...
go install golang.org/x/tools/cmd/guru
//如果需要在vscode中调试go程序，需要下载安装dlv
go install github.com/derekparker/delve/cmd/dlv